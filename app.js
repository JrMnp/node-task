
const express = require('express')
const app = express()
const port = 3000

/**
 * This function adds two numbers together
 * @param {Number} a first param
 * @param {Number} b second param
 * @returns {Number}
 */
const add = (a, b) => {
    return a + b;
}

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})